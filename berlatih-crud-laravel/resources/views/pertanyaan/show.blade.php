@extends('layouts.master')

@section('content')

<div class="card">
    <div class="card-header">
        <h3 class="card-title">Menampilkan Pertanyaan ke {{$pertanyaan->id}}</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <h4>{{$pertanyaan->judul}}</h4>
        <p>{{$pertanyaan->isi}}</p>
    </div>
    <!-- /.card-body -->
</div>
@endsection