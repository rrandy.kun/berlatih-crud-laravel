@extends('layouts.master')

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Daftar Pertanyaan</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
            <a href="/pertanyaan/create" class="btn btn-primary">Tambah</a>
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th>Judul</th>
                    <th>Isi</th>
                    <th>Tanggal Dibuat</th>
                    <th>Tanggal Diperbaharui</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($pertanyaan as $key=>$value)
                <tr>
                    <td>{{$key + 1}}</th>
                    <td>{{$value->judul}}</td>
                    <td>{{$value->isi}}</td>
                    <td>{{$value->tanggal_dibuat}}</td>
                    <td>{{$value->tanggal_diperbaharui}}</td>
                    <td>
                        <a href="/pertanyaan/{{$value->id}}" class="btn btn-info">Show</a>
                        <a href="/pertanyaan/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                        <form action="/pertanyaan/{{$value->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger my-1" value="Delete">
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="6" align="center">No data</td>
                </tr>
                @endforelse
            </tbody>
            <tfoot>
                <tr>
                    <th scope="col">#</th>
                    <th>Judul</th>
                    <th>Isi</th>
                    <th>Tanggal Dibuat</th>
                    <th>Tanggal Diperbaharui</th>
                    <th scope="col">Actions</th>
                </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.card-body -->
</div>
@endsection

@push('scripts')
<script src="{{asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
    $(function() {
        $("#example1").DataTable();
    });
</script>
@endpush